//
//  XMPPPCoreDataStack.m
//  XMPPPSDK
//
//  Created by Wei Ting Chen on 2014/8/13.
//  Copyright (c) 2014年 WeiTing Studio. All rights reserved.
//

#import "CoreDataStack.h"
#import <UIKit/UIKit.h>

@interface CoreDataStack()
{
    
}
@property (nonatomic, assign) BOOL initialized;

@end

@implementation CoreDataStack

NSString * const kShareStackDidInitialized = @"kShareStackDidInitialized";
@synthesize mainQueueObjectContext = _mainQueueObjectContext;
@synthesize writerObjectContext = _writerObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
static CoreDataStack *s_shareStack = nil;


#pragma mark - Core Data stack
+ (instancetype)shareStack
{
    if (!s_shareStack) {
        s_shareStack = [[CoreDataStack alloc] init];
        s_shareStack.initialized = NO;
    }
    return s_shareStack;
}
+ (void)clearInstance
{
    s_shareStack = nil;
}
- (void)initializeWithDatabaseFilename:(NSString *)filename
{
    if (!_initialized) {
        _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
        
        // setup persistent store coordinator
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:filename];
        NSError *error = nil;
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_managedObjectModel];
        
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
        {
            // handle error
            NSLog(@"addPersistentStoreWithType error");
        }
        
        // create writer MOC
        _writerObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_writerObjectContext setPersistentStoreCoordinator:_persistentStoreCoordinator];
        
        // create main queue MOC
        _mainQueueObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
       // [_mainQueueObjectContext setPersistentStoreCoordinator:_persistentStoreCoordinator];
        _mainQueueObjectContext.parentContext = _writerObjectContext;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kShareStackDidInitialized object:s_shareStack];
        });
        _initialized = YES;
    }
}
- (NSManagedObjectContext *)temporatoryObjectContext
{
    NSManagedObjectContext *context = nil;
    if (_initialized) {
        context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        //context.parentContext = self.mainQueueObjectContext;
        [context setPersistentStoreCoordinator:_persistentStoreCoordinator];
        
    }
    return context;
}
- (void)saveContext:(void (^)())complete
{
    if (_initialized) {
        NSManagedObjectContext *mainObjectContext = self.mainQueueObjectContext;
        NSManagedObjectContext *writerObjectContext = self.writerObjectContext;
        if (mainObjectContext != nil && writerObjectContext != nil) {
            [mainObjectContext performBlock:^{
                NSError *error = nil;
                if (![mainObjectContext save:&error]) {
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                    abort();
                }
                [writerObjectContext performBlock:^{
                    NSError *error = nil;
                    if (![writerObjectContext save:&error]) {
                        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                        abort();
                    }
                    if (complete) {
                        complete();
                    }
                }];
            }];
        } else {
            NSLog(@"writing context not exist");
            complete();
        }
    } else {
        NSLog(@"stack not initialized");
    }
}
- (NSManagedObjectContext *)mainQueueObjectContext
{
    if (_initialized) {
        return _mainQueueObjectContext;
    }
    return nil;
}
#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


-(BOOL)isClearEntity:(NSString *)entityName andContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9)
    {
        NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
        
        NSError *deleteError = nil;
        [[CoreDataStack shareStack].persistentStoreCoordinator executeRequest:delete withContext:context error:&deleteError];
        if (deleteError == nil)
        {
            return YES;
        }
    }else
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
        [request setEntity:entity];
        //[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        [request setFetchBatchSize:20];
        NSError *error = nil;
        NSArray *orders =  [context executeFetchRequest: request error:&error];
        for (NSManagedObject *order in orders)
        {
            [context deleteObject:order];
        }
        return YES;
    }
    return NO;
}

@end
