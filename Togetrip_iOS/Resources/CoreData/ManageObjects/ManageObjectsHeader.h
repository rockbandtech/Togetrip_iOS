//
//  ManageObjectsHeader.h
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#ifndef ManageObjectsHeader_h
#define ManageObjectsHeader_h

#define ENTITY_NAME_USER @"UserEntity"

#import "UserEntity+CoreDataClass.h"
#import "ScheduleEntity+CoreDataClass.h"
#import "DailySchedule+CoreDataClass.h"
#import "Location+CoreDataClass.h"

#endif /* ManageObjectsHeader_h */
