//
//  XMPPPCoreDataStack.h
//  XMPPPSDK
//
//  Created by Wei Ting Chen on 2014/8/13.
//  Copyright (c) 2014年 WeiTing Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CoreDataStack : NSObject
extern NSString * const kShareStackDidInitialized;


@property (readonly, strong, nonatomic) NSManagedObjectContext *mainQueueObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *writerObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
+ (instancetype)shareStack;
+ (void)clearInstance;
- (void)initializeWithDatabaseFilename:(NSString *)filename;
- (NSManagedObjectContext *)temporatoryObjectContext;
- (void)saveContext:(void (^)())complete;
- (NSURL *)applicationDocumentsDirectory;
-(BOOL)isClearEntity:(NSString *)entityName andContext:(NSManagedObjectContext *)context;
@end
