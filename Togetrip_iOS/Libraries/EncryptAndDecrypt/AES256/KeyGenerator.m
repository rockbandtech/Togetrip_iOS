//
//  KeyGenerator.m
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/12.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "KeyGenerator.h"
#import <NSHash/NSString+NSHash.h>
#import <NSHash/NSData+NSHash.h>

NSString* const letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

@implementation KeyGenerator

+ (instancetype) generator
{
    static KeyGenerator *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[KeyGenerator alloc] init];
    });
    return instance;
}

-(NSString *)generateAppKey
{
    NSString *shaUUID = [[UIDevice currentDevice].identifierForVendor.UUIDString SHA256];
    NSMutableString *shaString = [[NSMutableString alloc] initWithString:shaUUID];
    [shaString appendString:[self hexStringWithLength:128-shaUUID.length]];
    return shaString;
}

-(NSString *)aesPrivateKey:(NSString *)publicKey
{
    NSInteger tableInt[32] =
    {
        19,55,44,62,62,
        11,46,48,13,3,
        2,30,46,43,34,
        45,56,30,12,38,
        7,11,38,24,42,
        22,48,45,56,60,
        0,32
    };
    if (publicKey == nil)
    {
        return nil;
    }
    const char *keyChar = [publicKey UTF8String];
    NSMutableString *privateKey = [[NSMutableString alloc] init];
    for (NSInteger i = 0; i < 32; ++i)
    {
        NSInteger position = tableInt[i];
        [privateKey appendString:[NSString stringWithFormat:@"%c",keyChar[position]]];
    }
    return privateKey;
}
-(NSString *)hexStringWithLength: (NSInteger) length
{
    NSMutableString *randomString = [NSMutableString stringWithCapacity: length];
    
    for (NSInteger i=0; i<length; i++)
    {
        
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    return randomString;
}

@end
