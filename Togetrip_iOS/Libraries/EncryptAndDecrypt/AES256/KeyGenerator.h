//
//  KeyGenerator.h
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/12.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyGenerator : NSObject

+ (instancetype) generator;

-(NSString *)generateAppKey;
-(NSString *)aesPrivateKey:(NSString *)publicKey;
-(NSString *)hexStringWithLength: (NSInteger) length;

@end
