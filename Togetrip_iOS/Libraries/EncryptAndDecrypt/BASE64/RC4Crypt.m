//
//  RC4Crypt.m
//  TripittaVehicleDispatching_iOS
//
//  Created by Morris_Lin on 2016/3/17.
//  Copyright © 2016年 Fullerton. All rights reserved.
//

#import "RC4Crypt.h"

@interface RC4Crypt ()

@property (nonatomic, strong) NSArray * key;
@property (nonatomic, strong) NSMutableArray * sBox;

@end

static const NSInteger SBOX_LENGTH = 256;
//static const NSInteger KEY_MIN_LENGTH = 1;

@implementation RC4Crypt

+(RC4Crypt *)sharedInstance
{
    static RC4Crypt *rc4Crypt;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        rc4Crypt = [[RC4Crypt alloc] init];
    });
    return rc4Crypt;
}


-(NSString *)encrypt:(NSString *)string withKey:(NSString *)key
{
    self.sBox = [[self frameSBox:key] mutableCopy];
    unichar code[string.length];
    
    int i = 0;
    int j = 0;
    for (NSInteger n = 0; n < string.length; n++) {
        i = (i + 1) % SBOX_LENGTH;
        j = (j + [[self.sBox objectAtIndex:i]integerValue]) % SBOX_LENGTH;
        [self.sBox exchangeObjectAtIndex:i withObjectAtIndex:j];
        
        NSInteger index=([self.sBox[i] integerValue]+[self.sBox[j] integerValue]);
        
        NSInteger rand=([self.sBox[(index%SBOX_LENGTH)] integerValue]);
        
        code[n]=(rand  ^  (NSInteger)[string characterAtIndex:n]);
    }
    const unichar* buffer;
    buffer = code;
    
    return  [NSString stringWithCharacters:buffer length:string.length];
}
- (NSString*) decrypt:(NSString*)string withKey:(NSString*)key
{
    return [self encrypt:string withKey:key];
}
-(NSArray *)frameSBox:(NSString *)keyValue
{
    
    NSMutableArray *sBox = [[NSMutableArray alloc] initWithCapacity:SBOX_LENGTH];
    
    NSInteger j = 0;
    
    for (NSInteger i = 0; i < SBOX_LENGTH; i++) {
        [sBox addObject:[NSNumber numberWithInteger:i]];
    }
    
    for (NSInteger i = 0; i < SBOX_LENGTH; i++)
    {
        j = (j + [sBox[i] integerValue] + [keyValue characterAtIndex:(i % keyValue.length)]) % SBOX_LENGTH;
        [sBox exchangeObjectAtIndex:i withObjectAtIndex:j];
    }
    
    return [NSArray arrayWithArray:sBox];
}

@end
