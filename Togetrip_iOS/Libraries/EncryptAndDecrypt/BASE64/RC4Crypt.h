//
//  RC4Crypt.h
//  TripittaVehicleDispatching_iOS
//
//  Created by Morris_Lin on 2016/3/17.
//  Copyright © 2016年 Fullerton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RC4Crypt : NSObject

-(NSString *)encrypt:(NSString *)string withKey:(NSString *)key;
- (NSString*) decrypt:(NSString*)string withKey:(NSString*)key;
+(RC4Crypt *)sharedInstance;

@end
