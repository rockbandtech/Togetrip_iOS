//
//  ValidationViewController.m
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/13.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "ValidationViewController.h"
#import "SignModel.h"
#import "CheckInputLimit.h"
#import "KeyChainStore.h"
#import "AppDelegate.h"

@interface ValidationViewController ()<UITextFieldDelegate>
{
    __weak IBOutlet UITextField *inputTextField;
    __weak IBOutlet UIButton *validateButton;
    __weak IBOutlet UIView *successView;
    __weak IBOutlet UILabel *titleLabel;
    
    
    
}
@end

@implementation ValidationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Action
-(IBAction)textFieldTextDidChangeAction:(UITextField *)sender
{
    sender.text = [CheckInputLimit checkInputLimitByMaxLength:10 inputtedText:sender.text];
    if (sender.text.length == 10)
    {
        [self enableButtonClick:YES];
    }else
    {
        [self enableButtonClick:NO];
    }
    
}
-(IBAction)buttonClickAction:(UIButton *)sender
{
    [SignModel validateUser:inputTextField.text Email:_email complete:^(BOOL isSuccess, NSDictionary *errorInfo)
    {
        if (isSuccess && errorInfo == nil)
        {
            //登入
            [self.view bringSubviewToFront:successView];
            successView.hidden = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UITabBarController *tabBarController = (UITabBarController *) [storyboard instantiateViewControllerWithIdentifier:@"MainTabBarController"];
                [self.navigationController showViewController:tabBarController sender:nil];
            });
        }else
        {
            NSInteger errorCode = [errorInfo[@"errorCode"] integerValue];
            if (errorCode == 1001)
            {
                NSLog(@"%@",errorInfo[@"message"]);
            }
        }
    }];
}
#pragma mark - private
-(void)enableButtonClick:(BOOL)isEnable
{
    validateButton.enabled = isEnable;
    if (isEnable)
    {
        validateButton.alpha = 1.0;
    }else
    {
        validateButton.alpha = 0.3;
    }
}
#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
