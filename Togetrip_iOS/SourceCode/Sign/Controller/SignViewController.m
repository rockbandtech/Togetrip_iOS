//
//  SignViewController.m
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "SignViewController.h"
#import "SignModel.h"
#import "ValidationViewController.h"
#import "AppDelegate.h"
#import "SignBaseViewController.h"

@interface SignViewController ()<UITextFieldDelegate>
{
    
    __weak IBOutlet UITextField *inputTextField;
    __weak IBOutlet UIButton *signButton;
    SignBaseViewController *baseViewController;
}
@end

@implementation SignViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    baseViewController = (SignBaseViewController *)self.parentViewController;
    [baseViewController registerKeyBoardNotification];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:baseViewController];
}

#pragma mark - Action
-(IBAction)buttonClickAction:(UIButton *)sender
{
    
    SignObject *signObj = [SignObject new];
    signObj.appKey = (NSString *)[KeyChainStore load:KEYCHAIN_KEY_SIGNATURE];
    if (signObj.appKey == nil)
    {
        signObj.appKey = KeyGenerator.generator.generateAppKey;
    }
    signObj.email = [inputTextField.text AES256EncryptWithKey:[KeyGenerator.generator aesPrivateKey:signObj.appKey]];
    
    [SignModel signUser:signObj complete:^(BOOL isSuccess, NSDictionary *errorInfo)
     {
         if (isSuccess && errorInfo == nil)
         {
             //進入首頁
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                 UITabBarController *tabBarController = (UITabBarController *) [storyboard instantiateViewControllerWithIdentifier:@"MainTabBarController"];
                 [self.navigationController showViewController:tabBarController sender:nil];
                 
                 
             });
         }else if (!isSuccess && errorInfo == nil)
         {
             //驗證
             ValidationViewController *validateVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ValidationViewController"];
             validateVC.email = inputTextField.text;
             validateVC.appKey = signObj.appKey;
             [baseViewController basePushToViewController:validateVC Animated:YES];
             
         }else
         {
             //有錯
         }
     }];
}
-(IBAction)textFieldTextDidChangeAction:(UITextField *)sender
{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailPredicate evaluateWithObject:sender.text])
    {
        [self enableButtonClick:YES];
    }else
    {
        [self enableButtonClick:NO];
    }
}
#pragma mark - private
-(void)enableButtonClick:(BOOL)isEnable
{
    signButton.enabled = isEnable;
    if (isEnable)
    {
        signButton.alpha = 1.0;
    }else
    {
        signButton.alpha = 0.3;
    }
}
#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField endEditing:YES];
    return YES;
}
@end
