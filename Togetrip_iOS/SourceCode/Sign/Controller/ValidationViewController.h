//
//  ValidationViewController.h
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/13.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ValidationViewController : UIViewController

@property(copy,nonatomic)NSString *email;
@property(copy,nonatomic)NSString *appKey;

@end
