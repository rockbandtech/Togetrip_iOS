//
//  SignBaseViewController.h
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/9/25.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "BaseViewController.h"

@interface SignBaseViewController : BaseViewController

-(void)registerKeyBoardNotification;

@end
