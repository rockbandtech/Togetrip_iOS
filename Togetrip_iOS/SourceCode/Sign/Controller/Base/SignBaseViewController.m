//
//  SignBaseViewController.m
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/9/25.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "SignBaseViewController.h"

@interface SignBaseViewController ()
{
    __weak IBOutlet UIScrollView *baseScrollView;
    CGRect keyboardFrameBeginRect;
}
@end

@implementation SignBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    keyboardFrameBeginRect = CGRectZero;
}
#pragma mark - public
-(void)registerKeyBoardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveKeyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveKeyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
}
#pragma mark - Notification
-(void)receiveKeyboardWillShowNotification:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    if (keyboardFrameBeginRect.size.height == 0)
    {
        keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    }
    baseScrollView.scrollEnabled = YES;
    baseScrollView.contentSize = CGSizeMake(0, [[UIScreen mainScreen] bounds].size.height + keyboardFrameBeginRect.size.height);
}
-(void)receiveKeyboardWillHideNotification:(NSNotification *)notification
{
    baseScrollView.scrollEnabled = NO;
    baseScrollView.contentOffset = CGPointMake(0, 0);
    baseScrollView.contentSize = CGSizeMake(0, [[UIScreen mainScreen] bounds].size.height);
}

@end
