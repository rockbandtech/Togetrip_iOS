//
//  SignModel.h
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SignObject.h"
#import "KeyGenerator.h"

@interface SignModel : NSObject

+(void)signUser:(SignObject *)signObj complete:(void(^)(BOOL isSuccess, NSDictionary *errorInfo))completeBlock;
+(void)validateUser:(NSString *)validateCode Email:(NSString *)email complete:(void(^)(BOOL isSuccess, NSDictionary *errorInfo))completeBlock;

@end
