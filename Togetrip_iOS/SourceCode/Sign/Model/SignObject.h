//
//  SignObject.h
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignObject : NSObject

@property(copy,nonatomic)   NSString  *userId;
@property(copy,nonatomic)   NSString  *passportId;
@property(copy,nonatomic)   NSString  *accountName;
@property(assign,nonatomic) NSInteger accountType;
@property(copy,nonatomic)   NSString  *address;
@property(copy,nonatomic)   NSString  *email;
@property(copy,nonatomic)   NSDate    *birthday;
@property(copy,nonatomic)   NSString  *avatarUrl;
@property(assign,nonatomic) NSInteger gender;
@property(copy,nonatomic)   NSString  *cellPhone;
@property(copy,nonatomic)   NSString  *tel_home;
@property(copy,nonatomic)   NSString  *intorduction;
@property(copy,nonatomic)   NSString  *appKey;
@property(copy,nonatomic)   NSString  *token;
@property(copy,nonatomic)   NSString  *pushToken;

@end
