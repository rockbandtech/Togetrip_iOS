//
//  SignModel.m
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "SignModel.h"
#import "ApiModel.h"
#import "KeyChainStore.h"

@implementation SignModel

+(void)signUser:(SignObject *)signObj complete:(void(^)(BOOL isSuccess, NSDictionary *errorInfo))completeBlock
{
    [ApiModel postHttpRequestHeaderField:nil Parameters:signObj.attributes URL:@"api/user/sign" complete:^(BOOL isSuccess, id responseObj)
    {
        NSLog(@"傑森：%@",responseObj);
        NSInteger error = 0;
        if (isSuccess)
        {
            error = [responseObj[@"error"][@"errorCode"] integerValue];
            if (error == 0)
            {
                NSDictionary *userInfo = responseObj[@"userInfo"];
                if (userInfo)
                {
                    NSManagedObjectContext *context = [CoreDataStack shareStack].temporatoryObjectContext;
                    UserEntity *user = [NSEntityDescription insertNewObjectForEntityForName:ENTITY_NAME_USER inManagedObjectContext:context];
                    if ([[CoreDataStack shareStack] isClearEntity:ENTITY_NAME_USER andContext:context])
                    {
                        for (NSString *key in user.attributeNames)
                        {
                            if (userInfo[key])
                            {
                                [user setValue:userInfo[key] forKey:key];
                            }
                        }
                        [context save:nil];
                        [[CoreDataStack shareStack] saveContext:^{
                            [KeyChainStore save:KEYCHAIN_KEY_SIGNATURE data:signObj.appKey];
                            completeBlock (YES,nil);
                        }];
                    }
                }else
                {
                    //需要驗證
                    [[NSUserDefaults standardUserDefaults] setValue:signObj.appKey forKey:USERDEFAULT_KEY_APPKEY];
                    completeBlock (NO,nil);
                }
            }else
            {
                completeBlock(NO,responseObj[@"error"]);
            }

        }else
        {
            error = [responseObj[@"errorCode"] integerValue];
            completeBlock(NO,@{@"errorCode": responseObj[@"errorCode"]});
        }
    }];
}

+(void)validateUser:(NSString *)validateCode Email:(NSString *)email complete:(void(^)(BOOL isSuccess, NSDictionary *errorInfo))completeBlock
{
    NSString *appKey = [[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_KEY_APPKEY];
    NSDictionary *params = @{
                             @"validateCode": validateCode,
                             @"email"       : [email AES256EncryptWithKey:[KeyGenerator.generator aesPrivateKey:appKey]],
                             @"appKey"      : appKey
                             };
    [ApiModel postHttpRequestHeaderField:nil Parameters:params URL:@"api/user/validate" complete:^(BOOL isSuccess, id responseObj)
    {
        NSLog(@"******:%@",responseObj);
        NSInteger errorCode = 0;
        if (isSuccess)
        {
            errorCode = [responseObj[@"error"][@"errorCode"] integerValue];
            if (errorCode == 0)
            {
                NSManagedObjectContext *context = [CoreDataStack shareStack].temporatoryObjectContext;
                
                UserEntity *user = [NSEntityDescription insertNewObjectForEntityForName:ENTITY_NAME_USER inManagedObjectContext:context];
                NSDictionary *userInfo = responseObj[@"userInfo"];
                if (userInfo && [[CoreDataStack shareStack] isClearEntity:ENTITY_NAME_USER andContext:context])
                {
                    for (NSString *key in user.attributeNames)
                    {
                        if (userInfo[key])
                        {
                            [user setValue:userInfo[key] forKey:key];
                        }
                    }
                    [context save:nil];
                    [[CoreDataStack shareStack] saveContext:^{
                        [KeyChainStore save:KEYCHAIN_KEY_SIGNATURE data:appKey];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USERDEFAULT_KEY_APPKEY];
                        completeBlock (YES,nil);
                    }];
                }
                
            }else
            {
                completeBlock(NO,responseObj[@"error"]);
            }
        }else
        {
            completeBlock(NO,@{@"errorCode": responseObj[@"errorCode"]});
        }
        
    }];
}
@end
