//
//  BaseViewController.h
//  SmartHomeConnect
//
//  Created by Morris Lin on 2017/5/19.
//  Copyright © 2017年 JSW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

//params
@property (weak, nonatomic) IBOutlet UIView *baseNavigationBar;
@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *navLeftButton;
@property(strong,nonatomic)UIViewController *containerViewController;
//method
-(void)basePushToViewController:(UIViewController *)viewController Animated:(BOOL)anmated;
-(void)basePresentViewController:(UIViewController *)viewController Animated:(BOOL)anmated;
-(void)addTarget:(id)aTarget navButtonClickAction:(SEL)selector;
-(void)addTarget:(id)aTarget buttonClickAction:(SEL)selector;
-(void)hideBaseNavigationBar;
-(void)displayBaseNavigationBar;

@end
