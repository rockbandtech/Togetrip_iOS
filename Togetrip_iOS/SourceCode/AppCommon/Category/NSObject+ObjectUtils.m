//
//  NSObject+ObjectUtils.m
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "NSObject+ObjectUtils.h"
#import <objc/runtime.h>

@implementation NSObject (ObjectUtils)


- (NSArray *)attributeNames
{
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableArray *attributes = [NSMutableArray array];
    
    unsigned i;
    for (i = 0; i < count; i++)
    {
        objc_property_t property = properties[i];
        NSString *name = [NSString stringWithUTF8String:property_getName(property)];
        [attributes addObject:name];
    }
    
    free(properties);
    
    return attributes;
}
- (NSDictionary *)attributes
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
        /*
        else
            [dictionary setObject:[NSNull null] forKey:key];
         */
    }
    
    free(properties);
    
    return dictionary;
}

@end
