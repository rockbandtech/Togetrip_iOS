//
//  NSObject+ObjectUtils.h
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ObjectUtils)

- (NSArray *)attributeNames;
- (NSDictionary *)attributes;

@end
