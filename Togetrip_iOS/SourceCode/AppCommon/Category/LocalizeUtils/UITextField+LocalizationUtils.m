//
//  UITextField+LocalizationUtils.m
//  SmartHomeConnect
//
//  Created by Morris Lin on 2017/5/16.
//  Copyright © 2017年 JSW. All rights reserved.
//

#import "UITextField+LocalizationUtils.h"

@implementation UITextField (LocalizationUtils)

- (void)setLocalizedPlaceholder:(NSString*)localizedText
{
    self.placeholder = YCLocalizedString(localizedText, nil);
}

- (NSString*)localizedPlaceholder
{
    return self.placeholder;
}

@end
