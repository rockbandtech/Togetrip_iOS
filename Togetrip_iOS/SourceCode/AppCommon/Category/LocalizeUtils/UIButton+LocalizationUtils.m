//
//  UIButton+LocalizationUtils.m
//  SmartHomeConnect
//
//  Created by Morris Lin on 2017/5/16.
//  Copyright © 2017年 JSW. All rights reserved.
//

#import "UIButton+LocalizationUtils.h"

@implementation UIButton (LocalizationUtils)
-(void)setLocalizedTitle:(NSString *)localizedTitle
{
    [self setTitle:YCLocalizedString(localizedTitle, nil) forState:UIControlStateNormal];
}
-(NSString *)localizedTitle
{
    return self.titleLabel.text;
}


@end
