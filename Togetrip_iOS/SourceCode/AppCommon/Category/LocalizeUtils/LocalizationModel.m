//
//  LocalizationModel.m
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "LocalizationModel.h"

@implementation LocalizationModel

+(NSString *)localizedStringByKey:(NSString *)key Comment:(NSString *)comment
{
    return NSLocalizedStringFromTable(key, @"Localizable", comment);
}
+(NSString *)localizedTableForBundleId
{
    
    return [[NSBundle mainBundle].bundleIdentifier isEqualToString:@"Bundle id"] ?@"String Table Name" : @"Localizable";
}


@end
