//
//  UILabel+LocalizationUtils.m
//  SmartHomeConnect
//
//  Created by Morris Lin on 2017/5/16.
//  Copyright © 2017年 JSW. All rights reserved.
//

#import "UILabel+LocalizationUtils.h"

@implementation UILabel (LocalizationUtils)

- (void)setLocalizedText:(NSString*)localizedText
{
    self.text = YCLocalizedString(localizedText, nil);
}
- (NSString*)localizedText
{
    return self.text;
}
@end
