//
//  ApiModel.m
//  TripittaVehicleDispatching_iOS
//
//  Created by pomelolin48 on 2016/1/19.
//  Copyright © 2016年 Fullerton. All rights reserved.
//

#import "ApiModel.h"

@implementation ApiModel

+(void)postHttpRequestHeaderField:(NSDictionary *)headerField Parameters:(NSDictionary *)parameters  URL:(NSString *)url complete:(void (^)(BOOL, id))completeBlock
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:API_URL_DOMAIN]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if (headerField != nil)
    {
        [ApiModel setHTTPHeaderField:headerField :manager];
    }
    [manager POST:[NSString stringWithFormat:@"%@.json",url] parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        completeBlock(YES,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"\n============== ERROR ====\n%@",error.userInfo);
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        completeBlock(NO,@{@"errorCode" : @(response.statusCode)});
    }];
}
+(void)getHttpRequestHeaderField:(NSDictionary *)headerField Parameters:(NSDictionary *)parameters  URL:(NSString *)url complete:(void (^)(BOOL, id))completeBlock
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:API_URL_DOMAIN]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if (headerField != nil)
    {
        [ApiModel setHTTPHeaderField:headerField :manager];
    }
    [manager GET:[NSString stringWithFormat:@"%@.json",url] parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        completeBlock(YES,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"\n============== ERROR ====\n%@",error.userInfo);
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        completeBlock(NO,@{@"errorCode" : @(response.statusCode)});
    }];
}

+(void)putHttpRequestHeaderField:(NSDictionary *)headerField Parameters:(NSDictionary *)parameters  URL:(NSString *)url complete:(void (^)(BOOL, id))completeBlock
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:API_URL_DOMAIN]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if (headerField != nil)
    {
        [ApiModel setHTTPHeaderField:headerField :manager];
    }
    [manager PUT:[NSString stringWithFormat:@"%@.json",url] parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        completeBlock(YES,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"\n============== ERROR ====\n%@",error.userInfo);
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        completeBlock(NO,@{@"errorCode" : @(response.statusCode)});
    }];
}
+(void)deleteHttpRequestHeaderField:(NSDictionary *)headerField Parameters:(NSDictionary *)parameters  URL:(NSString *)url complete:(void (^)(BOOL, id))completeBlock
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:API_URL_DOMAIN]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if (headerField != nil)
    {
        [ApiModel setHTTPHeaderField:headerField :manager];
    }
    [manager DELETE:[NSString stringWithFormat:@"%@.json",url] parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        completeBlock(YES,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"\n============== ERROR ====\n%@",error.userInfo);
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        completeBlock(NO,@{@"errorCode" : @(response.statusCode)});
    }];
}
+(void)setHTTPHeaderField:(NSDictionary *)header :(AFHTTPSessionManager *)manager
{
    for (NSString *key in header)
    {
        [manager.requestSerializer setValue:[header objectForKey:key] forHTTPHeaderField:key];
    }
}

@end
