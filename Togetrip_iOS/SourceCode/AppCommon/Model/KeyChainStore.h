//
//  KeyChainStore.h
//  TripittaVehicleDispatching_iOS
//
//  Created by Morris_Lin on 2016/10/11.
//  Copyright © 2016年 Fullerton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyChainStore : NSObject

+ (void)save:(NSString *)service data:(id)data;
+ (id)load:(NSString *)service;
+ (void)deleteKeyData:(NSString *)service;

@end
