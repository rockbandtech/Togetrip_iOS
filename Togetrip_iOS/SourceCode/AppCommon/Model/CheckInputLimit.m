//
//  CheckInputLimit.m
//  FETECS
//
//  Created by pomelolin48 on 2015/5/5.
//  Copyright (c) 2015年 Zoaks Co., Ltd. All rights reserved.
//

#import "CheckInputLimit.h"
@interface CheckInputLimit()

@end
//static NSUInteger lastInputBytes;
@implementation CheckInputLimit
+(NSString *)checkInputLimitByMaxLength:(NSInteger)maxLength inputtedText:(NSString *)text
{
    if (text.length > maxLength)
    {
        text = [text substringToIndex:maxLength];
    }
    return text;
}
+(NSString *)checkInputLimitByMaxBytes:(NSUInteger)maxBytes inputtedText:(NSString *)text
{
    if (text.length > maxBytes)
    {
        text = [text substringToIndex:maxBytes];
    }
    NSUInteger bytes = [text lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    while (bytes > maxBytes)
    {
        text = [text substringToIndex:[text length]-1];
        bytes = [text lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    }
    return text;
}

@end
