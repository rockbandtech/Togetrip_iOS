//
//  ApiModel.h
//  TripittaVehicleDispatching_iOS
//
//  Created by pomelolin48 on 2016/1/19.
//  Copyright © 2016年 Fullerton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#define API_URL_DOMAIN @"https://togettrip-on-rails-dreamplanet.c9users.io/"

@interface ApiModel : NSObject

+(void)postHttpRequestHeaderField:(NSDictionary *)headerField Parameters:(NSDictionary *)parameters  URL:(NSString *)url complete:(void (^)(BOOL, id))completeBlock;
+(void)getHttpRequestHeaderField:(NSDictionary *)headerField Parameters:(NSDictionary *)parameters  URL:(NSString *)url complete:(void (^)(BOOL, id))completeBlock;
+(void)putHttpRequestHeaderField:(NSDictionary *)headerField Parameters:(NSDictionary *)parameters  URL:(NSString *)url complete:(void (^)(BOOL, id))completeBlock;
+(void)deleteHttpRequestHeaderField:(NSDictionary *)headerField Parameters:(NSDictionary *)parameters  URL:(NSString *)url complete:(void (^)(BOOL, id))completeBlock;

@end
