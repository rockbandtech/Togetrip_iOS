//
//  CheckInputLimit.h
//  FETECS
//
//  Created by pomelolin48 on 2015/5/5.
//  Copyright (c) 2015年 Zoaks Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckInputLimit : NSObject
+(NSString *)checkInputLimitByMaxLength:(NSInteger)maxLength inputtedText:(NSString *)text;
+(NSString *)checkInputLimitByMaxBytes:(NSUInteger)maxBytes inputtedText:(NSString *)text;

@end
