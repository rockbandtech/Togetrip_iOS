//
//  AppDefineHeader.h
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/12.
//  Copyright © 2017年 jsw. All rights reserved.
//


#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(...)
#endif
// ALog always displays output regardless of the DEBUG setting
#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);


#ifndef AppDefineHeader_h
#define AppDefineHeader_h

#define CORE_DATA_FILENAME @"TogetripDB.sqlite"
#define KEYCHAIN_KEY_SIGNATURE @"keychain.key.signature"
#define USERDEFAULT_KEY_APPKEY @"userdefault.key.appkey"

#endif /* AppDefineHeader_h */
