//
//  SearchBarView.m
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/29.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "SearchBarView.h"



@implementation SearchBarView

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        if (self.subviews.count == 0)
        {
            view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
            view.frame = self.bounds;
            view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [self addSubview:view];
        }
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    view.backgroundColor = self.backgroundColor;
}

@end
