//
//  UIAvatarView.m
//  CustomViewTest
//
//  Created by Morris Lin on 2017/5/23.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "UIAvatarView.h"

@interface UIAvatarView ()
{
    UIImage *avatarImage;
    __weak IBOutlet UIImageView *avatarImageView;
}

@property (strong, nonatomic) IBOutlet UIView *view;

@end

@implementation UIAvatarView

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        if (self.subviews.count == 0)
        {
            _view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
            _view.frame = self.bounds;
            _view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [self addSubview:_view];
        }
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    _view.backgroundColor = self.backgroundColor;
    _view.layer.borderColor = self.layer.borderColor;
    _view.layer.cornerRadius = _view.frame.size.height/2;
    self.layer.cornerRadius = _view.layer.cornerRadius;
    [self drawImageView];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}
-(void)drawImageView
{
    CGFloat newMultiplier = (self.bounds.size.width - _innerWidth*2)/self.bounds.size.width;
    for (NSLayoutConstraint *constraint in _view.constraints)
    {
        if ([constraint.identifier isEqualToString:@"equalWidthConstrint"])
        {
            NSLayoutConstraint *oldConstraint = constraint;
            NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:oldConstraint.firstItem attribute:oldConstraint.firstAttribute relatedBy:oldConstraint.relation toItem:oldConstraint.secondItem attribute:oldConstraint.secondAttribute multiplier:newMultiplier constant:oldConstraint.constant];
            newConstraint.priority = oldConstraint.priority;
            newConstraint.identifier = constraint.identifier;
            [_view removeConstraint:oldConstraint];
            [_view addConstraint:newConstraint];
        }
    }
    [self.view layoutIfNeeded];
    avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height/2;
    avatarImageView.layer.masksToBounds = YES;
}
/*
-(void)drawImageView
{
    avatarImageView.frame = CGRectMake(0, 0, _view.frame.size.width - _innerWidth * 2, _view.frame.size.height - _innerWidth * 2);
    
    avatarImageView.center = _view.center;
    avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height/2;
    avatarImageView.layer.masksToBounds = YES;
}
*/
#pragma mark - public
-(void)putAvatarImage:(UIImage *)image
{
    avatarImageView.image = image;
}
-(void)putAvatarUrlImage:(NSString *)url
{
    [avatarImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:_imageName]];
}
#pragma mark - override
-(void)setImageName:(NSString *)imageName
{
    avatarImageView.image = [UIImage imageNamed:imageName];
}
@end
