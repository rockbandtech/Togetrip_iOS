//
//  ScheduleObject.h
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/9/1.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ScheduleObject : NSObject

@property(copy,nonatomic) NSString *userId;
@property(strong,nonatomic) ScheduleEntity *schdlEntity;


@end
