//
//  ScheduleModel.h
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/9/1.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScheduleModel : NSObject

+(void)createNewSchedule:(NSDictionary *)params complete:(void(^)(BOOL isSuccess, NSDictionary *errorInfo))completeBlock;


@end
