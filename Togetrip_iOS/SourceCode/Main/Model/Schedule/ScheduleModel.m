//
//  ScheduleModel.m
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/9/1.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "ScheduleModel.h"
#import "ApiModel.h"

@implementation ScheduleModel

+(void)createNewSchedule:(NSDictionary *)params complete:(void(^)(BOOL isSuccess, NSDictionary *errorInfo))completeBlock
{
    [ApiModel postHttpRequestHeaderField:nil Parameters:params URL:@"api/schedule/create" complete:^(BOOL isSuccess, id responseObj)
    {
        if (isSuccess)
        {
            NSLog(@"%@",responseObj);
        }else
        {
            DLog(@"GG");
        }
    }];
}


@end
