//
//  JourneyCollectionViewCell.h
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/30.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAvatarView.h"
@interface JourneyCollectionViewCell : UICollectionViewCell

//journeysCell
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UIAvatarView *avatarView;

@end
