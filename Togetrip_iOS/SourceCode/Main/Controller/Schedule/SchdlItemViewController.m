//
//  ScheduleViewController.m
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/9/1.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "SchdlItemViewController.h"
#import "ScheduleModel.h"


@interface SchdlItemViewController ()

@end

@implementation SchdlItemViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Action
-(IBAction)buttonClickAction:(UIButton *)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:Nil];
    [self.navigationController pushViewController:[storyboard
                                                   instantiateViewControllerWithIdentifier: @"SCHDLBaseViewController"] animated:YES];
}
@end
