//
//  JCollectionViewController.m
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/30.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "JCollectionViewController.h"
#import "JourneyCollectionViewCell.h"

@interface JCollectionViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
}

@end

@implementation JCollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return 2;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.restorationIdentifier isEqualToString:@"JourneyLocation"])
    {
        return CGSizeMake(80, 60);
    }
    if ([self.restorationIdentifier isEqualToString:@"JourneyFriends"])
    {
        return CGSizeMake(60, 80);
    }
    if ([self.restorationIdentifier isEqualToString:@"JourneyJourneys"])
    {
        return CGSizeMake(self.view.bounds.size.width, 155);
    }
    if ([self.restorationIdentifier isEqualToString:@"JourneySpots"])
    {
        return CGSizeMake((self.view.bounds.size.width-20)/2, 180);
    }
    return CGSizeZero;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdentifier = nil;
    if ([self.restorationIdentifier isEqualToString:@"JourneyLocation"])
    {
        reuseIdentifier = @"locationCell";
    }
    if ([self.restorationIdentifier isEqualToString:@"JourneyFriends"])
    {
        reuseIdentifier = @"friendsCell";
    }
    if ([self.restorationIdentifier isEqualToString:@"JourneyJourneys"])
    {
        reuseIdentifier = @"journeysCell";
    }
    if ([self.restorationIdentifier isEqualToString:@"JourneySpots"])
    {
        reuseIdentifier = @"spotsCell";
    }
    JourneyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (![reuseIdentifier isEqualToString:@"friendsCell"])
    {
        cell.layer.cornerRadius = 5.0;
    }
    //UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
    
    // Configure the cell
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */


@end
