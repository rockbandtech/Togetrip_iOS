//
//  JourneyViewController.m
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/30.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "JourneyViewController.h"
#import "JCollectionViewController.h"
@interface JourneyViewController ()

@end

@implementation JourneyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"From:%@",segue.identifier);
    JCollectionViewController *collectionVC = (JCollectionViewController *)[segue destinationViewController];
    if ([segue.identifier isEqualToString:@"displayLocation"])
    {
        NSLog(@"1");
        collectionVC.displayArray = nil;
        return;
    }
    if ([segue.identifier isEqualToString:@"displayFriends"])
    {
        NSLog(@"2");
        collectionVC.displayArray = nil;
        return;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
