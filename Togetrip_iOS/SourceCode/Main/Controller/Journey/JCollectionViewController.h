//
//  JCollectionViewController.h
//  Togetrip_iOS
//
//  Created by pomelolin48 on 2017/7/30.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JCollectionViewController : UIViewController

@property(strong,nonatomic) NSArray *displayArray;

@end
