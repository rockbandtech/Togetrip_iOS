//
//  SCHDLBaseViewController.m
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/9/4.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "SCHDLBaseViewController.h"

@interface SCHDLBaseViewController ()

@end

static NSManagedObjectContext *sContext = nil;

@implementation SCHDLBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - public

-(NSManagedObjectContext *)getScheduleContext
{
    if (sContext == nil)
    {
        sContext = [CoreDataStack shareStack].temporatoryObjectContext;
    }
    return sContext;
}

-(void)releaseContext
{
    @autoreleasepool
    {
        sContext = nil;
    }
}
#pragma mark - Action
-(IBAction)backButtonClickAction:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
