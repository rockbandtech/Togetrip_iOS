//
//  SCHDLBaseViewController.h
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/9/4.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "BaseViewController.h"

@interface SCHDLBaseViewController : BaseViewController

-(NSManagedObjectContext *)getScheduleContext;
-(void)releaseContext;

@end
