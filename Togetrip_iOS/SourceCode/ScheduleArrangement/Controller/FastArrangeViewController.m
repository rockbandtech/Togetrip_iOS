//
//  FastArrangeViewController.m
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/9/7.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import "FastArrangeViewController.h"
#import "LXReorderableCollectionViewFlowLayout.h"
#import "FACollectionViewCell.h"

@interface FastArrangeViewController ()<LXReorderableCollectionViewDataSource, LXReorderableCollectionViewDelegateFlowLayout>
{
    
    __weak IBOutlet UICollectionView *kCollectionView;
}
@end

@implementation FastArrangeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ((LXReorderableCollectionViewFlowLayout *)kCollectionView.collectionViewLayout).minimumLineSpacing = 10.0;
    ((LXReorderableCollectionViewFlowLayout *)kCollectionView.collectionViewLayout).minimumInteritemSpacing = 0.0;
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellWidth = (collectionView.bounds.size.width-20)/3.0;
    return CGSizeMake(cellWidth, cellWidth);
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FACollectionViewCell" forIndexPath:indexPath];
    return cell;
}
#pragma mark - LXReorderableCollectionViewDataSource methods
- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath willMoveToIndexPath:(NSIndexPath *)toIndexPath
{
}
- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
#if LX_LIMITED_MOVEMENT == 1
    
#else
    return YES;
#endif
}

- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath
{
#if LX_LIMITED_MOVEMENT == 1
#else
    return YES;
#endif
}

#pragma mark - LXReorderableCollectionViewDelegateFlowLayout methods

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"will begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"will end drag");
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did end drag");
}
@end
