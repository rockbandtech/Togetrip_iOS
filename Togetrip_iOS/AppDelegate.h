//
//  AppDelegate.h
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
+(instancetype)shareAppDelegate;

@end

